package net.ihe.gazelle.patient;

import net.ihe.gazelle.hql.HibernateHelper;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import org.jboss.seam.annotations.Name;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Random;

/**
 * <p>AbstractPatient class.</p>
 *
 * @author aberge
 * @version $Id: $Id
 */

@Entity
@Name("abstractPatient")
@XmlRootElement
@Table(name = "pat_patient", schema = "public", uniqueConstraints = @UniqueConstraint(columnNames = "id"))
@SequenceGenerator(name = "pat_patient_sequence", sequenceName = "pat_patient_id_seq", allocationSize = 1)
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public class AbstractPatient implements Serializable {

    private static final long serialVersionUID = 1813894504257097185L;

    @Id
    @GeneratedValue(generator = "pat_patient_sequence", strategy = GenerationType.SEQUENCE)
    @Column(name = "id", nullable = false, unique = true)
    @NotNull
    private Integer id;

    @Column(name = "first_name")
    @XmlElement(name = "firstname")
    protected String firstName;

    @Column(name = "last_name")
    @XmlElement(name = "lastname")
    protected String lastName;

    @Column(name = "alternate_first_name")
    @XmlElement(name = "alternatefirstname")
    protected String alternateFirstName;

    @Column(name = "alternate_last_name")
    @XmlElement(name = "alternatelastname")
    protected String alternateLastName;

    @Column(name = "mother_maiden_name")
    @XmlElement(name = "motherMaidenName")
    protected String motherMaidenName;

    @Column(name = "gender_code")
    @XmlElement(name = "genderCode")
    protected String genderCode;

    @Column(name = "religion_code")
    protected String religionCode;

    @Column(name = "race_code")
    protected String raceCode;

    @Column(name = "date_of_birth")
    @Temporal(TemporalType.TIMESTAMP)
    @XmlElement(name = "dateOfBirth")
    protected Date dateOfBirth;

    @Transient
    protected String dateOfBirthyyyyMMdd;

    @Column(name = "country_code")
    @XmlElement(name = "countryCode")
    protected String countryCode;

    @Transient
    protected String nationalPatientIdentifier;

    @Transient
    protected String ddsIdentifier;

    @Column(name = "character_set")
    protected String characterSet;

    @Column(name = "creation_date")
    @Temporal(TemporalType.TIMESTAMP)
    protected Date creationDate;

    @Column(name = "alternate_mothers_maiden_name")
    protected String alternateMothersMaidenName;

    @Column(name = "second_name")
    protected String secondName;

    @Column(name = "third_name")
    protected String thirdName;

    @Column(name = "alternate_second_name")
    protected String alternateSecondName;

    @Column(name = "alternate_third_name")
    protected String alternateThirdName;

    @Column(name = "weight")
    protected Integer weight;

    @Column(name="size")
    protected Integer size;

    @OneToMany(mappedBy = "patient", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    protected List<PatientAddress> addressList;

    @Transient
    private PatientAddress mainAddress;

    /**
     * Constructor
     */
    public AbstractPatient() {
        this.weight = getRandomWeight();
        this.size = getRandomSize();
        this.creationDate = new Date();
    }

    /**
     * <p>getPatientAge.</p>
     *
     * @return a {@link java.lang.Integer} object.
     */
    public Integer getPatientAge(){
        if (dateOfBirth != null){
           Calendar today = new GregorianCalendar();
           int thisYear = today.get(Calendar.YEAR);
           int thisMonth = today.get(Calendar.MONTH);
           Calendar birthDate = new GregorianCalendar();
           birthDate.setTime(dateOfBirth);
           int birthYear = birthDate.get(Calendar.YEAR);
           int birthMonth = birthDate.get(Calendar.MONTH);
           int age = thisYear - birthYear;
           if (thisMonth < birthMonth){
               return age - 1;
           } else {
               return age;
           }
        } else {
            return null;
        }
    }

    private Integer getRandomWeight() {
        Random r = new Random();
        return 1 + r.nextInt(149);
    }

    private Integer getRandomSize(){
        Random r = new Random();
        return 50 + r.nextInt(150);
    }

    /**
     * <p>Getter for the field <code>firstName</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * <p>Setter for the field <code>firstName</code>.</p>
     *
     * @param firstName a {@link java.lang.String} object.
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * <p>Getter for the field <code>lastName</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * <p>Setter for the field <code>lastName</code>.</p>
     *
     * @param lastName a {@link java.lang.String} object.
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * <p>Getter for the field <code>motherMaidenName</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getMotherMaidenName() {
        return motherMaidenName;
    }

    /**
     * <p>Setter for the field <code>motherMaidenName</code>.</p>
     *
     * @param motherMaidenName a {@link java.lang.String} object.
     */
    public void setMotherMaidenName(String motherMaidenName) {
        this.motherMaidenName = motherMaidenName;
    }

    /**
     * <p>Getter for the field <code>genderCode</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getGenderCode() {
        return genderCode;
    }

    /**
     * <p>Setter for the field <code>genderCode</code>.</p>
     *
     * @param genderCode a {@link java.lang.String} object.
     */
    public void setGenderCode(String genderCode) {
        this.genderCode = genderCode;
    }

    /**
     * <p>Getter for the field <code>religionCode</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getReligionCode() {
        return religionCode;
    }

    /**
     * <p>Setter for the field <code>religionCode</code>.</p>
     *
     * @param religionCode a {@link java.lang.String} object.
     */
    public void setReligionCode(String religionCode) {
        this.religionCode = religionCode;
    }

    /**
     * <p>Getter for the field <code>raceCode</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getRaceCode() {
        return raceCode;
    }

    /**
     * <p>Setter for the field <code>raceCode</code>.</p>
     *
     * @param raceCode a {@link java.lang.String} object.
     */
    public void setRaceCode(String raceCode) {
        this.raceCode = raceCode;
    }

    /**
     * <p>Getter for the field <code>dateOfBirth</code>.</p>
     *
     * @return a {@link java.util.Date} object.
     */
    public Date getDateOfBirth() {
        if (dateOfBirth == null && dateOfBirthyyyyMMdd != null) {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
            try {
                this.dateOfBirth = sdf.parse(this.dateOfBirthyyyyMMdd);
            } catch (ParseException e) {
                return null;
            }
        }
        return this.dateOfBirth;
    }

    /**
     * <p>Setter for the field <code>dateOfBirth</code>.</p>
     *
     * @param dateOfBirth a {@link java.lang.String} object.
     */
    public void setDateOfBirth(String dateOfBirth) {
        if (dateOfBirth != null) {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
            try {
                this.dateOfBirth = sdf.parse(dateOfBirth);
            } catch (ParseException e) {
                this.dateOfBirth = null;
            }
        }
    }

    /**
     * <p>Setter for the field <code>dateOfBirth</code>.</p>
     *
     * @param dateOfBirth a {@link java.util.Date} object.
     */
    public void setDateOfBirth(Date dateOfBirth) {
        if (dateOfBirth != null) {
            this.dateOfBirth = (Date) dateOfBirth.clone();
        } else {
            this.dateOfBirth = null;
        }
    }

    /**
     * <p>Getter for the field <code>countryCode</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getCountryCode() {
        return countryCode;
    }

    /**
     * <p>Setter for the field <code>countryCode</code>.</p>
     *
     * @param countryCode a {@link java.lang.String} object.
     */
    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    /**
     * <p>getAddressLine.</p>
     *
     * @return a {@link java.lang.String} object.
     * @since 2.2.0
     */
    public String getAddressLine() {
        PatientAddress mainAddress = getMainAddress();
        if (mainAddress != null) {
            return mainAddress.getAddressLine();
        } else {
            return null;
        }
    }

    /**
     * <p>getStreet.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    @Deprecated
    public String getStreet() {
        PatientAddress mainAddress = getMainAddress();
        if (mainAddress != null) {
            return mainAddress.getStreet();
        } else {
            return null;
        }
    }

    /**
     * <p>getZipCode.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getZipCode() {
        PatientAddress mainAddress = getMainAddress();
        if (mainAddress != null) {
            return mainAddress.getZipCode();
        } else {
            return null;
        }
    }

    /**
     * <p>getCity.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getCity() {
        PatientAddress mainAddress = getMainAddress();
        if (mainAddress != null) {
            return mainAddress.getCity();
        } else {
            return null;
        }
    }

    /**
     * <p>getState.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getState() {
        PatientAddress mainAddress = getMainAddress();
        if (mainAddress != null) {
            return mainAddress.getState();
        } else {
            return null;
        }
    }

    /**
     * <p>Getter for the field <code>characterSet</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getCharacterSet() {
        return characterSet;
    }

    /**
     * <p>Setter for the field <code>characterSet</code>.</p>
     *
     * @param characterSet a {@link java.lang.String} object.
     */
    public void setCharacterSet(String characterSet) {
        this.characterSet = characterSet;
    }

    /**
     * <p>Getter for the field <code>nationalPatientIdentifier</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getNationalPatientIdentifier() {
        return nationalPatientIdentifier;
    }

    /**
     * <p>Setter for the field <code>nationalPatientIdentifier</code>.</p>
     *
     * @param nationalPatientIdentifier a {@link java.lang.String} object.
     */
    public void setNationalPatientIdentifier(String nationalPatientIdentifier) {
        this.nationalPatientIdentifier = nationalPatientIdentifier;
    }

    /**
     * <p>Getter for the field <code>ddsIdentifier</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getDdsIdentifier() {
        return ddsIdentifier;
    }

    /**
     * <p>Setter for the field <code>ddsIdentifier</code>.</p>
     *
     * @param ddsIdentifier a {@link java.lang.String} object.
     */
    public void setDdsIdentifier(String ddsIdentifier) {
        this.ddsIdentifier = ddsIdentifier;
    }

    /**
     * <p>Setter for the field <code>creationDate</code>.</p>
     *
     * @param creationDate a {@link java.util.Date} object.
     */
    public void setCreationDate(Date creationDate) {
        if (creationDate != null) {
            this.creationDate = (Date) creationDate.clone();
        } else {
            this.creationDate = null;
        }
    }

    /**
     * <p>Getter for the field <code>creationDate</code>.</p>
     *
     * @return a {@link java.util.Date} object.
     */
    public Date getCreationDate() {
        if (creationDate != null) {
            return (Date) creationDate.clone();
        } else {
            return null;
        }
    }

    /** {@inheritDoc} */
    @Override
    public int hashCode() {
        return HibernateHelper.getLazyHashcode(this);
    }

    /** {@inheritDoc} */
    @Override
    public boolean equals(Object obj) {
        return HibernateHelper.getLazyEquals(this, obj);
    }

    /**
     * <p>Getter for the field <code>alternateFirstName</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getAlternateFirstName() {
        return alternateFirstName;
    }

    /**
     * <p>Setter for the field <code>alternateFirstName</code>.</p>
     *
     * @param alternateFirstName a {@link java.lang.String} object.
     */
    public void setAlternateFirstName(String alternateFirstName) {
        this.alternateFirstName = alternateFirstName;
    }

    /**
     * <p>Getter for the field <code>alternateLastName</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getAlternateLastName() {
        return alternateLastName;
    }

    /**
     * <p>Setter for the field <code>alternateLastName</code>.</p>
     *
     * @param alternateLastName a {@link java.lang.String} object.
     */
    public void setAlternateLastName(String alternateLastName) {
        this.alternateLastName = alternateLastName;
    }

    /**
     * <p>Getter for the field <code>dateOfBirthyyyyMMdd</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getDateOfBirthyyyyMMdd() {
        return dateOfBirthyyyyMMdd;
    }

    /**
     * <p>Setter for the field <code>dateOfBirthyyyyMMdd</code>.</p>
     *
     * @param dateOfBirthyyyyMMdd a {@link java.lang.String} object.
     */
    public void setDateOfBirthyyyyMMdd(String dateOfBirthyyyyMMdd) {
        this.dateOfBirthyyyyMMdd = dateOfBirthyyyyMMdd;
    }

    /**
     * <p>Getter for the field <code>alternateMothersMaidenName</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getAlternateMothersMaidenName() {
        return alternateMothersMaidenName;
    }

    /**
     * <p>Setter for the field <code>alternateMothersMaidenName</code>.</p>
     *
     * @param alternateMothersMaidenName a {@link java.lang.String} object.
     */
    public void setAlternateMothersMaidenName(String alternateMothersMaidenName) {
        this.alternateMothersMaidenName = alternateMothersMaidenName;
    }

    /**
     * <p>Getter for the field <code>secondName</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getSecondName() {
        return secondName;
    }

    /**
     * <p>Setter for the field <code>secondName</code>.</p>
     *
     * @param secondName a {@link java.lang.String} object.
     */
    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    /**
     * <p>Getter for the field <code>thirdName</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getThirdName() {
        return thirdName;
    }

    /**
     * <p>Setter for the field <code>thirdName</code>.</p>
     *
     * @param thirdName a {@link java.lang.String} object.
     */
    public void setThirdName(String thirdName) {
        this.thirdName = thirdName;
    }

    /**
     * <p>Getter for the field <code>alternateSecondName</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getAlternateSecondName() {
        return alternateSecondName;
    }

    /**
     * <p>Setter for the field <code>alternateSecondName</code>.</p>
     *
     * @param alternateSecondName a {@link java.lang.String} object.
     */
    public void setAlternateSecondName(String alternateSecondName) {
        this.alternateSecondName = alternateSecondName;
    }

    /**
     * <p>Getter for the field <code>alternateThirdName</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getAlternateThirdName() {
        return alternateThirdName;
    }

    /**
     * <p>Setter for the field <code>alternateThirdName</code>.</p>
     *
     * @param alternateThirdName a {@link java.lang.String} object.
     */
    public void setAlternateThirdName(String alternateThirdName) {
        this.alternateThirdName = alternateThirdName;
    }

    /**
     * <p>Getter for the field <code>addressList</code>.</p>
     *
     * @return a {@link java.util.List} object.
     * @since 2.2.0
     */
    public List<PatientAddress> getAddressList() {
        if (addressList == null) {
            addressList = new ArrayList<PatientAddress>();
        }
        return addressList;
    }

    /**
     * <p>Setter for the field <code>addressList</code>.</p>
     *
     * @param addressList a {@link java.util.List} object.
     * @since 2.2.0
     */
    public void setAddressList(List<PatientAddress> addressList) {
        this.addressList = addressList;
    }

    /**
     * <p>getAddressType.</p>
     *
     * @return a {@link net.ihe.gazelle.patient.AddressType} object.
     * @since 2.2.0
     */
    public AddressType getAddressType() {
        if (getMainAddress() != null) {
            return getMainAddress().getAddressType();
        } else {
            return null;
        }
    }

    /**
     * <p>addPatientAddress.</p>
     *
     * @param address a {@link net.ihe.gazelle.patient.PatientAddress} object.
     * @since 2.2.0
     */
    public void addPatientAddress(PatientAddress address) {
        if (address != null) {
            getAddressList().add(address);
        }
    }

    /**
     * <p>removePatientAddress.</p>
     *
     * @param address a {@link net.ihe.gazelle.patient.PatientAddress} object.
     * @since 2.2.0
     */
    public void removePatientAddress(PatientAddress address) {
        if (address != null) {
            getAddressList().remove(address);
            if (address.getId() != null) {
                EntityManager entityManager = EntityManagerService.provideEntityManager();
                entityManager.remove(address);
            }
        }
    }

    /**
     * <p>Getter for the field <code>mainAddress</code>.</p>
     *
     * @return a {@link net.ihe.gazelle.patient.PatientAddress} object.
     * @since 2.2.0
     */
    public PatientAddress getMainAddress() {
        if (mainAddress != null) {
            return mainAddress;
        } else if (id != null) {
            PatientAddressQuery query = new PatientAddressQuery();
            query.patient().id().eq(this.getId());
            query.mainAddress().eq(true);
            mainAddress = query.getUniqueResult();
        } else {
            for (PatientAddress address : getAddressList()) {
                if (address.isMainAddress()) {
                    mainAddress = address;
                    break;
                }
            }
        }
        return mainAddress;
    }

    /**
     * <p>copyAddressList.</p>
     *
     * @param patient a {@link net.ihe.gazelle.patient.AbstractPatient} object.
     * @since 2.2.0
     */
    protected final void copyAddressList(AbstractPatient patient) {
        if (patient != null) {
            for (PatientAddress address : patient.getAddressList()) {
                if (address != null && address.isDefined()) {
                    PatientAddress patientAddress = new PatientAddress(address);
                    patientAddress.setPatient(this);
                    addPatientAddress(patientAddress);
                }
            }
        }
    }

    /**
     * <p>Getter for the field <code>id</code>.</p>
     *
     * @return a {@link java.lang.Integer} object.
     * @since 2.2.0
     */
    public Integer getId() {
        return id;
    }

    /**
     * <p>Setter for the field <code>id</code>.</p>
     *
     * @param id a {@link java.lang.Integer} object.
     * @since 2.2.0
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * <p>Getter for the field <code>weight</code>.</p>
     *
     * @return a {@link java.lang.Integer} object.
     */
    public Integer getWeight() {
        return weight;
    }

    /**
     * <p>Setter for the field <code>weight</code>.</p>
     *
     * @param weight a {@link java.lang.Integer} object.
     */
    public void setWeight(Integer weight) {
        this.weight = weight;
    }

    /**
     * <p>Getter for the field <code>size</code>.</p>
     *
     * @return a {@link java.lang.Integer} object.
     */
    public Integer getSize() {
        return size;
    }

    /**
     * <p>Setter for the field <code>size</code>.</p>
     *
     * @param size a {@link java.lang.Integer} object.
     */
    public void setSize(Integer size) {
        this.size = size;
    }
}
