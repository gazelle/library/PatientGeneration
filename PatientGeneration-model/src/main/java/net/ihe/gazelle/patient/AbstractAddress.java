package net.ihe.gazelle.patient;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;
import javax.xml.bind.annotation.XmlElement;
import java.io.Serializable;

/**
 * Created by aberge on 03/07/17.
 */

@Entity
@Table(schema = "public", name = "pat_patient_address", uniqueConstraints = @UniqueConstraint(columnNames = {"id"}))
@SequenceGenerator(name = "pat_patient_address_sequence", sequenceName = "pat_patient_address_id_seq", allocationSize = 1)
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public class AbstractAddress implements Serializable{

    @Id
    @GeneratedValue(generator = "pat_patient_address_sequence", strategy = GenerationType.SEQUENCE)
    @Column(name = "id", nullable = false, unique = true)
    private Integer id;

    @Column(name = "address_type")
    private AddressType addressType;


    @Column(name = "country_code")
    @XmlElement(name = "countryCode")
    private String countryCode;

    @Column(name = "street")
    @XmlElement(name = "street")
    private String street;

    @Column(name = "street_number")
    @XmlElement(name = "streetNumber")
    private String streetNumber;

    @Column(name = "zip_code")
    @XmlElement(name = "zipCode")
    private String zipCode;

    @Column(name = "city")
    @XmlElement(name = "city")
    private String city;

    @Column(name = "address_line")
    @XmlElement(name = "addressLine")
    private String addressLine;

    @Column(name = "state")
    @XmlElement(name = "state")
    private String state;

    @Column(name = "insee_code")
    private String inseeCode;

    @Transient
    private Integer fakeId;

    private static Integer shareFakeId = 0;

    private void setFakeId() {
        synchronized (AbstractAddress.class) {
            this.fakeId = shareFakeId++;
        }
    }

    /**
     * <p>Constructor for PatientAddress.</p>
     */
    public AbstractAddress() {
        this.addressType = AddressType.HOME;
        setFakeId();
    }

    public AbstractAddress(AbstractAddress inPatientAddress){
        this.countryCode = inPatientAddress.getCountryCode();
        this.zipCode = inPatientAddress.getZipCode();
        this.state = inPatientAddress.getState();
        this.addressLine = inPatientAddress.getAddressLine();
        this.street = inPatientAddress.getStreet();
        this.streetNumber = inPatientAddress.getStreetNumber();
        this.addressType = inPatientAddress.getAddressType();
        setFakeId();
        this.city = inPatientAddress.getCity();
        this.inseeCode = inPatientAddress.getInseeCode();
    }

    public AbstractAddress(AddressType inAddressType){
        setFakeId();
        this.addressType = inAddressType;
    }

    /**
     * <p>Getter for the field <code>id</code>.</p>
     *
     * @return a {@link java.lang.Integer} object.
     */
    public Integer getId() {
        return id;
    }

    /**
     * <p>Setter for the field <code>id</code>.</p>
     *
     * @param id a {@link java.lang.Integer} object.
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * <p>Getter for the field <code>addressType</code>.</p>
     *
     * @return a {@link net.ihe.gazelle.patient.AddressType} object.
     */
    public AddressType getAddressType() {
        return addressType;
    }

    /**
     * <p>Setter for the field <code>addressType</code>.</p>
     *
     * @param addressType a {@link net.ihe.gazelle.patient.AddressType} object.
     */
    public void setAddressType(AddressType addressType) {
        this.addressType = addressType;
    }

    /**
     * <p>Getter for the field <code>countryCode</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getCountryCode() {
        return countryCode;
    }

    /**
     * <p>Setter for the field <code>countryCode</code>.</p>
     *
     * @param countryCode a {@link java.lang.String} object.
     */
    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    /**
     * <p>Getter for the field <code>street</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    @Deprecated
    public String getStreet() {
        return street;
    }

    /**
     * <p>Setter for the field <code>street</code>.</p>
     *
     * @param street a {@link java.lang.String} object.
     */
    @Deprecated
    public void setStreet(String street) {
        this.street = street;
    }

    /**
     * <p>Getter for the field <code>streetNumber</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getStreetNumber() {
        return streetNumber;
    }

    /**
     * <p>Setter for the field <code>streetNumber</code>.</p>
     *
     * @param streetNumber a {@link java.lang.String} object.
     */
    public void setStreetNumber(String streetNumber) {
        this.streetNumber = streetNumber;
    }

    /**
     * <p>Getter for the field <code>zipCode</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getZipCode() {
        return zipCode;
    }

    /**
     * <p>Setter for the field <code>zipCode</code>.</p>
     *
     * @param zipCode a {@link java.lang.String} object.
     */
    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    /**
     * <p>Getter for the field <code>city</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getCity() {
        return city;
    }

    /**
     * <p>Setter for the field <code>city</code>.</p>
     *
     * @param city a {@link java.lang.String} object.
     */
    public void setCity(String city) {
        this.city = city;
    }

    /**
     * <p>Getter for the field <code>addressLine</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getAddressLine() {
        return addressLine;
    }

    /**
     * <p>Setter for the field <code>addressLine</code>.</p>
     *
     * @param addressLine a {@link java.lang.String} object.
     */
    public void setAddressLine(String addressLine) {
        this.addressLine = addressLine;
    }

    /**
     * <p>Getter for the field <code>state</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getState() {
        return state;
    }

    /**
     * <p>Setter for the field <code>state</code>.</p>
     *
     * @param state a {@link java.lang.String} object.
     */
    public void setState(String state) {
        this.state = state;
    }

    public String getInseeCode() {
        return inseeCode;
    }

    public void setInseeCode(String inseeCode) {
        this.inseeCode = inseeCode;
    }

    /**
     * <p>Getter for the field <code>fakeId</code>.</p>
     *
     * @return a {@link java.lang.Integer} object.
     */
    public Integer getFakeId() {
        return fakeId;
    }

    /**
     * <p>isBirthPlace.</p>
     *
     * @return a boolean.
     */
    public boolean isBirthPlace() {
        return AddressType.BIRTH.equals(this.addressType);
    }

    /**
     * if none of the fields or only the addressType is set, returns false
     *
     * @return a boolean.
     */
    public boolean isDefined() {
        return (this.addressType != null && this.hashCode() != this.addressType.hashCode()) || this.hashCode() != 0;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof AbstractAddress)) {
            return false;
        }

        AbstractAddress that = (AbstractAddress) o;

        if (addressType != that.addressType) {
            return false;
        }
        if (countryCode != null ? !countryCode.equals(that.countryCode) : that.countryCode != null) {
            return false;
        }
        if (street != null ? !street.equals(that.street) : that.street != null) {
            return false;
        }
        if (streetNumber != null ? !streetNumber.equals(that.streetNumber) : that.streetNumber != null) {
            return false;
        }
        if (zipCode != null ? !zipCode.equals(that.zipCode) : that.zipCode != null) {
            return false;
        }
        if (city != null ? !city.equals(that.city) : that.city != null) {
            return false;
        }
        if (addressLine != null ? !addressLine.equals(that.addressLine) : that.addressLine != null) {
            return false;
        }
        return state != null ? state.equals(that.state) : that.state == null;
    }

    @Override
    public int hashCode() {
        int result = addressType != null ? addressType.hashCode() : 0;
        result = 31 * result + (countryCode != null ? countryCode.hashCode() : 0);
        result = 31 * result + (street != null ? street.hashCode() : 0);
        result = 31 * result + (streetNumber != null ? streetNumber.hashCode() : 0);
        result = 31 * result + (zipCode != null ? zipCode.hashCode() : 0);
        result = 31 * result + (city != null ? city.hashCode() : 0);
        result = 31 * result + (addressLine != null ? addressLine.hashCode() : 0);
        result = 31 * result + (state != null ? state.hashCode() : 0);
        return result;
    }
}
