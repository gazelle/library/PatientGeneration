package net.ihe.gazelle.patient.test;

import net.ihe.gazelle.patient.AbstractPatient;

import java.lang.reflect.Field;

public class TestMethods {

    public static void main(String[] args) {
        Field[] fields = AbstractPatient.class.getDeclaredFields();
        for (Field field : fields) {
            System.out.println(field.getName());
        }
    }

}
