package net.ihe.gazelle.patient;

import net.ihe.gazelle.common.interfacegenerator.GenerateInterface;
import net.ihe.gazelle.demographic.ws.CountryCode;
import net.ihe.gazelle.demographic.ws.DemographicDataServerServiceStub;
import net.ihe.gazelle.demographic.ws.GetListCountryCode;
import net.ihe.gazelle.demographic.ws.GetListCountryCodeE;
import net.ihe.gazelle.demographic.ws.GetListCountryCodeResponseE;
import net.ihe.gazelle.demographic.ws.IOExceptionException;
import net.ihe.gazelle.demographic.ws.JDOMExceptionException;
import net.ihe.gazelle.demographic.ws.Patient;
import net.ihe.gazelle.demographic.ws.ReturnAddress;
import net.ihe.gazelle.demographic.ws.ReturnAddressE;
import net.ihe.gazelle.demographic.ws.ReturnAddressResponseE;
import net.ihe.gazelle.demographic.ws.ReturnCountryInseeCode;
import net.ihe.gazelle.demographic.ws.ReturnCountryInseeCodeE;
import net.ihe.gazelle.demographic.ws.ReturnCountryInseeCodeResponseE;
import net.ihe.gazelle.demographic.ws.ReturnPatientWithAllOptions;
import net.ihe.gazelle.demographic.ws.ReturnPatientWithAllOptionsE;
import net.ihe.gazelle.demographic.ws.ReturnPatientWithAllOptionsResponseE;
import net.ihe.gazelle.demographic.ws.ReturnSimplePatient;
import net.ihe.gazelle.demographic.ws.ReturnSimplePatientE;
import net.ihe.gazelle.demographic.ws.ReturnSimplePatientResponseE;
import net.ihe.gazelle.demographic.ws.ReturnTownInseeCode;
import net.ihe.gazelle.demographic.ws.ReturnTownInseeCodeE;
import net.ihe.gazelle.demographic.ws.ReturnTownInseeCodeResponseE;
import net.ihe.gazelle.demographic.ws.SOAPExceptionException;
import net.ihe.gazelle.patient.action.PatientAddressEditor;
import net.ihe.gazelle.preferences.PreferenceService;
import org.apache.axis2.AxisFault;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;

/**
 * <p>PatientGenerator class.</p>
 *
 * @author aberge
 * @version $Id: $Id
 */
@GenerateInterface(value = "PatientGeneratorLocal", isLocal = false)
public class PatientGenerator extends PatientAddressEditor implements PatientGeneratorLocal, Serializable {

    private static final String EMPTY = "empty";
    private static final String MINIMAL = "minimal";
    private static final String FEMALE_CODE = "F";
    private static final String MALE_CODE = "M";
    private static final String FEMALE = "Female";
    private static final String MALE = "Male";
    private static final String UNKNOWN_CODE = "U";
    private static final Logger log = LoggerFactory.getLogger(PatientGenerator.class);

    private static final String RANDOM = "random";
    private static final Object LIKE = "like";
    private static final String EXACT = "exact";
    /** Constant <code>UNKNOWN="Unknown"</code> */
    public static final String UNKNOWN = "Unknown";

    private ReturnPatientWithAllOptions returnedPatient;
    protected DemographicDataServerServiceStub ddsStub;
    private List<String> availableCountries;
    private String selectedCountryString = null;
    private List<CountryCode> allCountries;
    private CountryCode selectedCountry = null;
    private String firstNameGenerationMode = RANDOM;
    private String lastNameGenerationMode = RANDOM;
    private String motherMaidenNameGenerationMode = RANDOM;
    private String addressGenerationMode = RANDOM;
    private String religionGenerationMode = RANDOM;
    private String raceGenerationMode = RANDOM;
    private String firstName;
    private String lastName;
    private String dateOfBirthGenerationMode = RANDOM;
    private String ddsMode;
    private boolean qualifiedIdentity = false;

    public void setReturnedPatient(ReturnPatientWithAllOptions ddsPatient) {
        this.returnedPatient = ddsPatient;
    }

    /**
     * <p>Getter for the field <code>returnedPatient</code>.</p>
     *
     * @return a {@link net.ihe.gazelle.demographic.ws.ReturnPatientWithAllOptions} object.
     */
    public ReturnPatientWithAllOptions getReturnedPatient() {
        return returnedPatient;
    }

    public void setAvailableCountries(List<String> availableCountries) {
        this.availableCountries = availableCountries;
    }

    /**
     * <p>Getter for the field <code>availableCountries</code>.</p>
     *
     * @return a {@link java.util.List} object.
     */
    public List<String> getAvailableCountries() {
        return availableCountries;
    }

    /**
     * <p>Getter for the field <code>selectedCountry</code>.</p>
     *
     * @return a {@link net.ihe.gazelle.demographic.ws.CountryCode} object.
     */
    public CountryCode getSelectedCountry() {
        return selectedCountry;
    }

    public void setSelectedCountry(CountryCode selectedCountry) {
        this.selectedCountry = selectedCountry;
    }

    /**
     * <p>Getter for the field <code>firstNameGenerationMode</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getFirstNameGenerationMode() {
        return firstNameGenerationMode;
    }

    public void setFirstNameGenerationMode(String firstNameGenerationMode) {
        this.firstNameGenerationMode = firstNameGenerationMode;
    }

    /**
     * <p>Getter for the field <code>lastNameGenerationMode</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getLastNameGenerationMode() {
        return lastNameGenerationMode;
    }

    public void setLastNameGenerationMode(String lastNameGenerationMode) {
        this.lastNameGenerationMode = lastNameGenerationMode;
    }

    /**
     * <p>Getter for the field <code>motherMaidenNameGenerationMode</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getMotherMaidenNameGenerationMode() {
        return motherMaidenNameGenerationMode;
    }

    public void setMotherMaidenNameGenerationMode(String motherMaidenNameGenerationMode) {
        this.motherMaidenNameGenerationMode = motherMaidenNameGenerationMode;
    }

    /**
     * <p>Getter for the field <code>addressGenerationMode</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getAddressGenerationMode() {
        return addressGenerationMode;
    }

    public void setAddressGenerationMode(String addressGenerationMode) {
        this.addressGenerationMode = addressGenerationMode;
    }

    /**
     * <p>Getter for the field <code>religionGenerationMode</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getReligionGenerationMode() {
        return religionGenerationMode;
    }

    public void setReligionGenerationMode(String religionGenerationMode) {
        this.religionGenerationMode = religionGenerationMode;
    }

    /**
     * <p>Getter for the field <code>raceGenerationMode</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getRaceGenerationMode() {
        return raceGenerationMode;
    }

    public void setRaceGenerationMode(String raceGenerationMode) {
        this.raceGenerationMode = raceGenerationMode;
    }

    /**
     * <p>Getter for the field <code>firstName</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * <p>Getter for the field <code>lastName</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setAllCountries(List<CountryCode> allCountries) {
        this.allCountries = allCountries;
    }

    /**
     * <p>Getter for the field <code>allCountries</code>.</p>
     *
     * @return a {@link java.util.List} object.
     */
    public List<CountryCode> getAllCountries() {
        return allCountries;
    }

    /**
     * <p>Getter for the field <code>dateOfBirthGenerationMode</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getDateOfBirthGenerationMode() {
        return dateOfBirthGenerationMode;
    }

    public void setDateOfBirthGenerationMode(String dateOfBirthGenerationMode) {
        this.dateOfBirthGenerationMode = dateOfBirthGenerationMode;
    }

    public void setSelectedCountryString(String selectedCountryString) {
        this.selectedCountryString = selectedCountryString;
    }

    public boolean isQualifiedIdentity() {
        return qualifiedIdentity;
    }

    public void setQualifiedIdentity(boolean qualifiedIdentity) {
        this.qualifiedIdentity = qualifiedIdentity;
    }

    /**
     * <p>Getter for the field <code>selectedCountryString</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getSelectedCountryString() {
        return selectedCountryString;
    }

    /**
     * <p>getDdsWsEndpoint.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getDdsWsEndpoint() {
        return PreferenceService.getString("dds_ws_endpoint");
    }

    /**
     * <p>initializeGenerator.</p>
     */
    public void initializeGenerator() {
        String ddsEndpoint = getDdsWsEndpoint();
        if (ddsEndpoint != null) {
            try {
                ddsStub = new DemographicDataServerServiceStub(ddsEndpoint);
                ddsStub._getServiceClient().getOptions().setProperty(org.apache.axis2.Constants.Configuration.DISABLE_SOAP_ACTION, true);
                getListOfAvailableCountries();
            } catch (AxisFault e) {
                log.error("Cannot create stub for DDS Webservice", e);
            }
        } else {
            log.error("The URL of DDS endpoint is not set, please create a preference with name dds_ws_endpoint");
        }
    }

    /**
     * <p>getListOfAvailableCountries.</p>
     */
    public void getListOfAvailableCountries() {
        returnedPatient = new ReturnPatientWithAllOptions();
        GetListCountryCode param = new GetListCountryCode();
        GetListCountryCodeE paramE = new GetListCountryCodeE();
        paramE.setGetListCountryCode(param);
        try {
            GetListCountryCodeResponseE responseE = ddsStub.getListCountryCode(paramE);
            if (responseE != null) {
                allCountries = Arrays.asList(responseE.getGetListCountryCodeResponse().get_return());
                if (allCountries != null && !allCountries.isEmpty()) {
                    availableCountries = new ArrayList<String>();
                    for (CountryCode country : allCountries) {
                        availableCountries.add(country.getPrintableName());
                    }
                    Collections.sort(availableCountries);
                }
            } else {
                allCountries = null;
            }
        } catch (RemoteException e) {
            log.error("Failed to get available countries", e);
            setAvailableCountries(null);
        }
    }

    /**
     * <p>generatePatient.</p>
     *
     * @return a patient
     */
    public AbstractPatient generatePatient() {
        if (selectedCountry == null) {
            log.debug("No country selected, take one randomly");
            int numberOfCountries = allCountries.size();
            int randomIndex = (int) (Math.random() * (numberOfCountries - 1));
            selectedCountryString = availableCountries.get(randomIndex);
            selectCountry();
        }
        Patient generatedPatient = null;
        if (getDdsMode().equals(MINIMAL)) {
            firstNameGenerationMode = RANDOM;
            lastNameGenerationMode = RANDOM;
            motherMaidenNameGenerationMode = RANDOM;
            addressGenerationMode = RANDOM;
            religionGenerationMode = EMPTY;
            raceGenerationMode = EMPTY;
            firstName = null;
            lastName = null;
            dateOfBirthGenerationMode = RANDOM;
            generatedPatient = generatePatientInMinimalMode();
        } else {
            generatedPatient = generatePatientWithAllOption();
        }
        if (generatedPatient != null) {
            AbstractPatient abstractPatient = new AbstractPatient();
            if (generatedPatient.getCountry() != null) {
                abstractPatient.setCountryCode(generatedPatient.getCountry().getIso3());
                if (generatedPatient.getCountry().getCharacterSetEncodingReference() != null) {
                    abstractPatient.setCharacterSet(generatedPatient.getCountry().getCharacterSetEncodingReference()
                            .getCharset());
                }
            }
            if (getOptionFromGenerationMode(addressGenerationMode)) {
                setPatientAddresses(generatedPatient, abstractPatient);
            }
            if (getOptionFromGenerationMode(dateOfBirthGenerationMode)) {
                Calendar dob = generatedPatient.getPerson().getDateOfBirth();
                abstractPatient.setDateOfBirth(dob.getTime());
            }
            if (getOptionFromGenerationMode(raceGenerationMode) && generatedPatient.getPerson().getRace() != null) {
                abstractPatient.setRaceCode(generatedPatient.getPerson().getRace().getCode());
            } else {
                abstractPatient.setRaceCode(null);
            }
            if (getOptionFromGenerationMode(religionGenerationMode)
                    && generatedPatient.getPerson().getReligion() != null) {
                abstractPatient.setReligionCode(generatedPatient.getPerson().getReligion().getCode());
            } else {
                abstractPatient.setReligionCode(null);
            }
            if (getOptionFromGenerationMode(motherMaidenNameGenerationMode)
                    && generatedPatient.getPerson().getMotherMaidenName() != null) {
                abstractPatient.setMotherMaidenName(generatedPatient.getPerson().getMotherMaidenName().getValue());
            }
            // gender is contained in generatedPatient
            if (generatedPatient.getPerson().getFirstNameSex() != null) {
                if (generatedPatient.getPerson().getFirstNameSex().getGender().getDescription().equals(FEMALE)) {
                    abstractPatient.setGenderCode(FEMALE_CODE);
                } else {
                    abstractPatient.setGenderCode(MALE_CODE);
                }
            }
            // gender is not contained in generatedPatient but is selected in the GUI
            else if (returnedPatient.getGenderDescription() != null
                    && returnedPatient.getGenderDescription().equals(FEMALE)) {
                abstractPatient.setGenderCode(FEMALE_CODE);
            } else if (returnedPatient.getGenderDescription() != null
                    && returnedPatient.getGenderDescription().equals(MALE)) {
                abstractPatient.setGenderCode(MALE_CODE);
            } else {
                abstractPatient.setGenderCode(UNKNOWN_CODE);
            }
            if ((firstNameGenerationMode.equals(RANDOM) || firstNameGenerationMode.equals(LIKE))
                    && generatedPatient.getPerson().getFirstNameSex() != null) {
                abstractPatient.setFirstName(generatedPatient.getPerson().getFirstNameSex().getFirstName());
            } else {
                abstractPatient.setFirstName(generatedPatient.getPerson().getFirstAlternativeName());
            }
            if ((lastNameGenerationMode.equals(RANDOM) || lastNameGenerationMode.equals(LIKE))
                    && generatedPatient.getPerson().getLastName() != null) {
                abstractPatient.setLastName(generatedPatient.getPerson().getLastName().getValue());
            } else {
                abstractPatient.setLastName(generatedPatient.getPerson().getLastAlternativeName());
            }
            abstractPatient.setNationalPatientIdentifier(generatedPatient.getNationalPatientIdentifier());
            abstractPatient.setDdsIdentifier(generatedPatient.getDdsPatientIdentifier());
            abstractPatient.setCreationDate(new Date());
            return abstractPatient;
        } else {
            return null;
        }
    }

    public PatientAddress generateAddress(String countryCode, boolean birthPlace){
        ReturnAddress ddsAddress = new ReturnAddress();
        ddsAddress.setCountryCode(countryCode);
        ddsAddress.setBirthPlace(birthPlace);
        ReturnAddressE addressE = new ReturnAddressE();
        addressE.setReturnAddress(ddsAddress);

        try {
            ReturnAddressResponseE responseE = ddsStub.returnAddress(addressE);
            if (responseE != null && responseE.getReturnAddressResponse() != null) {
                return newPatientAddressFromDDS(responseE.getReturnAddressResponse().get_return());
            } else {
                return null;
            }
        } catch (Exception e) {
            log.error("Failed to generate patient", e);
        }
        return null;
    }

    public String getCountryInseeCode(String countryCodeIso2){
        ReturnCountryInseeCode returnCountryInseeCode = new ReturnCountryInseeCode();
        returnCountryInseeCode.setCountryCode(countryCodeIso2);

        ReturnCountryInseeCodeE countryInseeCodeE = new ReturnCountryInseeCodeE();
        countryInseeCodeE.setReturnCountryInseeCode(returnCountryInseeCode);

        try {
            ReturnCountryInseeCodeResponseE responseE = ddsStub.returnCountryInseeCode(countryInseeCodeE);
            if (responseE != null && responseE.getReturnCountryInseeCodeResponse() != null) {
                return responseE.getReturnCountryInseeCodeResponse().get_return();
            } else {
                return null;
            }
        } catch (Exception e) {
            log.error("Failed to get INSEE code", e);
        }
        return null;
    }

    public String getTownInseeCode(String townName){
        ReturnTownInseeCode returnTownInseeCode = new ReturnTownInseeCode();
        returnTownInseeCode.setTownName(townName);

        ReturnTownInseeCodeE  townInseeCodeE = new ReturnTownInseeCodeE();
        townInseeCodeE.setReturnTownInseeCode(returnTownInseeCode);

        try {
            ReturnTownInseeCodeResponseE responseE = ddsStub.returnTownInseeCode(townInseeCodeE);
            if (responseE != null && responseE.getReturnTownInseeCodeResponse() != null) {
                return responseE.getReturnTownInseeCodeResponse().get_return();
            } else {
                return null;
            }
        } catch (Exception e) {
            log.error("Failed to get INSEE code", e);
        }
        return null;
    }

    private void setPatientAddresses(Patient generatedPatient, AbstractPatient abstractPatient) {
        boolean mainAddress = true;
        for (net.ihe.gazelle.demographic.ws.PatientAddress ddsAddress : generatedPatient.getAddress()) {
            PatientAddress patAddress = newPatientAddressFromDDS(ddsAddress);
            patAddress.setMainAddress(mainAddress);
            abstractPatient.addPatientAddress(patAddress);
            mainAddress = false;
        }
    }

    private PatientAddress newPatientAddressFromDDS(net.ihe.gazelle.demographic.ws.PatientAddress ddsAddress) {
        PatientAddress address = new PatientAddress();
        if (ddsAddress.getCountry() != null) {
            address.setCountryCode(ddsAddress.getCountry().getIso3());
        }
        if (ddsAddress.getCity() != null) {
            address.setCity(ddsAddress.getCity().getValue());
        }
        if (ddsAddress.getState() != null) {
            address.setState(ddsAddress.getState().getName());
        }
        address.setZipCode(ddsAddress.getPostalCode());
        if (ddsAddress.getStreet() != null) {
            address.setAddressLine(ddsAddress.getStreet().getValue());
        }
        address.setStreetNumber(ddsAddress.getNumber());
        return address;
    }

    /**
     * <p>generatePatientInMinimalMode.</p>
     *
     * @return a {@link net.ihe.gazelle.demographic.ws.Patient} object.
     */
    protected Patient generatePatientInMinimalMode() {
        ReturnSimplePatient returnSimplePatient = new ReturnSimplePatient();
        returnSimplePatient.setCountryCode(selectedCountry.getIso());
        ReturnSimplePatientE returnSimplePatientE = new ReturnSimplePatientE();
        returnSimplePatientE.setReturnSimplePatient(returnSimplePatient);
        try {
            ReturnSimplePatientResponseE response = ddsStub.returnSimplePatient(returnSimplePatientE);
            if (response != null && response.getReturnSimplePatientResponse() != null) {
                return response.getReturnSimplePatientResponse().get_return();
            } else {
                return null;
            }
        } catch (Exception e) {
            log.error("Failed to generate patient", e);
            return null;
        }
    }

    /**
     * <p>generatePatientWithAllOption.</p>
     *
     * @return a {@link net.ihe.gazelle.demographic.ws.Patient} object.
     */
    protected Patient generatePatientWithAllOption() {
        if (returnedPatient != null) {
            returnedPatient.setCountryCode(selectedCountry.getIso());
            returnedPatient.setFirstNameOption(getOptionFromGenerationMode(firstNameGenerationMode));
            if (firstNameGenerationMode.equals(LIKE)) {
                returnedPatient.setFirstNameLike(firstName);
            } else {
                returnedPatient.setFirstNameLike(null);
            }
            returnedPatient.setLastNameOption(getOptionFromGenerationMode(lastNameGenerationMode));
            if (lastNameGenerationMode.equals(LIKE)) {
                returnedPatient.setLastNameLike(lastName);
            } else {
                returnedPatient.setLastNameLike(null);
            }
            if (returnedPatient.getGenderDescription() == null || returnedPatient.getGenderDescription().isEmpty()) {
                returnedPatient.setGenderDescription(RANDOM);
            }
            returnedPatient.setAddressOption(getOptionFromGenerationMode(addressGenerationMode));
            returnedPatient.setBirthDayOption(getOptionFromGenerationMode(dateOfBirthGenerationMode));

            // Generate only if it is activated for the selected Country.
            if (selectedCountry.getGenerateRace()) {
                returnedPatient.setRaceOption(getOptionFromGenerationMode(raceGenerationMode));
            } else {
                returnedPatient.setRaceOption(false);
            }

            if (selectedCountry.getGenerateReligion()) {
                returnedPatient.setReligionOption(getOptionFromGenerationMode(religionGenerationMode));
            } else {
                returnedPatient.setReligionOption(false);
            }

            returnedPatient.setMotherMaidenNameOption(getOptionFromGenerationMode(motherMaidenNameGenerationMode));

            // New values Initialization after the DDS update
            returnedPatient.setMaritalSatusOption(UNKNOWN);
            returnedPatient.setDeadPatientOption(false);
            returnedPatient.setMaidenNameOption(false);
            returnedPatient.setAliasNameOption(false);
            returnedPatient.setDisplayNameOption(false);
            returnedPatient.setNewBornOption(false);

            if (firstNameGenerationMode.equals(EXACT)) {
                returnedPatient.setFirstNameIs(firstName);
            } else {
                returnedPatient.setFirstNameIs(null);
            }

            if (lastNameGenerationMode.equals(EXACT)) {
                returnedPatient.setLastNameIs(lastName);
            } else {
                returnedPatient.setLastNameIs(null);
            }

            ReturnPatientWithAllOptionsE ddsPatientE = new ReturnPatientWithAllOptionsE();
            ddsPatientE.setReturnPatientWithAllOptions(returnedPatient);

            try {
                ReturnPatientWithAllOptionsResponseE responseE = ddsStub.returnPatientWithAllOptions(ddsPatientE);
                if (responseE != null && responseE.getReturnPatientWithAllOptionsResponse() != null) {
                    return responseE.getReturnPatientWithAllOptionsResponse().get_return();

                } else {
                    return null;
                }
            } catch (Exception e) {
                log.error("Failed to generate patient", e);
            }
        }
        return null;
    }

    /**
     * Calls DDS to get a patient only based on country code (selectedCountry attribute must be set)
     *
     * @return a Patient
     */
    public AbstractPatient generatePatientRandomly() {
        firstNameGenerationMode = RANDOM;
        lastNameGenerationMode = RANDOM;
        motherMaidenNameGenerationMode = RANDOM;
        addressGenerationMode = RANDOM;
        religionGenerationMode = EMPTY;
        raceGenerationMode = EMPTY;
        firstName = null;
        lastName = null;
        dateOfBirthGenerationMode = RANDOM;
        return generatePatient();
    }

    /**
     * Calls DDS to get a patient only based on country code (selectedCountry attribute must be set)
     *
     * @return a Patient
     */
    public AbstractPatient generatePatientRandomly2() {
        if (selectedCountry == null) {
            log.error("No country selected");
            return null;
        }
        returnedPatient = new ReturnPatientWithAllOptions();
        returnedPatient.setCountryCode(selectedCountry.getIso());
        returnedPatient.setAddressOption(true);
        returnedPatient.setBirthDayOption(true);

        // Generate only if it is activated for the selected Country.
        if (selectedCountry.getGenerateRace()) {
            returnedPatient.setRaceOption(true);
        } else {
            returnedPatient.setRaceOption(false);
        }

        if (selectedCountry.getGenerateReligion()) {
            returnedPatient.setReligionOption(true);
        } else {
            returnedPatient.setReligionOption(false);
        }

        returnedPatient.setMotherMaidenNameOption(true);
        returnedPatient.setFirstNameOption(true);
        returnedPatient.setLastNameOption(true);
        returnedPatient.setFirstNameLike(null);
        returnedPatient.setLastNameLike(null);

        // New values Initialization after the DDS update
        returnedPatient.setMaritalSatusOption(UNKNOWN);
        returnedPatient.setDeadPatientOption(false);
        returnedPatient.setMaidenNameOption(false);
        returnedPatient.setAliasNameOption(false);
        returnedPatient.setDisplayNameOption(false);
        returnedPatient.setNewBornOption(false);

        ReturnPatientWithAllOptionsE returnedPatientE = new ReturnPatientWithAllOptionsE();
        returnedPatientE.setReturnPatientWithAllOptions(returnedPatient);
        try {
            ReturnPatientWithAllOptionsResponseE responseE = ddsStub.returnPatientWithAllOptions(returnedPatientE);
            if (responseE != null && responseE.getReturnPatientWithAllOptionsResponse() != null) {
                Patient generatedPatient = responseE.getReturnPatientWithAllOptionsResponse().get_return();
                AbstractPatient abstractPatient = new AbstractPatient();
                if (generatedPatient.getCountry() != null) {
                    abstractPatient.setCountryCode(generatedPatient.getCountry().getIso3());
                    if (generatedPatient.getCountry().getCharacterSetEncodingReference() != null) {
                        abstractPatient.setCharacterSet(generatedPatient.getCountry()
                                .getCharacterSetEncodingReference().getCharset());
                    }
                }

                setPatientAddresses(generatedPatient, abstractPatient);

                if (generatedPatient.getPerson().getDateOfBirth() != null) {
                    Calendar dob = generatedPatient.getPerson().getDateOfBirth();
                    abstractPatient.setDateOfBirth(dob.getTime());
                }
                if (generatedPatient.getPerson().getRace() != null) {
                    abstractPatient.setRaceCode(generatedPatient.getPerson().getRace().getCode());
                } else {
                    abstractPatient.setRaceCode(null);
                }
                if (generatedPatient.getPerson().getReligion() != null) {
                    abstractPatient.setReligionCode(generatedPatient.getPerson().getReligion().getCode());
                } else {
                    abstractPatient.setReligionCode(null);
                }
                if (generatedPatient.getPerson().getMotherMaidenName() != null) {
                    abstractPatient.setMotherMaidenName(generatedPatient.getPerson().getMotherMaidenName().getValue());
                }
                if (generatedPatient.getPerson().getFirstNameSex() != null) {
                    abstractPatient.setFirstName(generatedPatient.getPerson().getFirstNameSex().getFirstName());
                }
                if (generatedPatient.getPerson().getLastName() != null) {
                    abstractPatient.setLastName(generatedPatient.getPerson().getLastName().getValue());
                }
                abstractPatient.setNationalPatientIdentifier(generatedPatient.getNationalPatientIdentifier());
                abstractPatient.setDdsIdentifier(generatedPatient.getDdsPatientIdentifier());
                abstractPatient.setCreationDate(new Date());
                return abstractPatient;
            } else {
                return null;
            }
        } catch (SOAPExceptionException|IOExceptionException|JDOMExceptionException|RemoteException e) {
            log.error("Failed to get generated patient");
        }
        return null;
    }

    /**
     * The patient attribute is required if generation mode is equal to random or like
     *
     * @param generationMode : string type of generation
     * @return boolean
     */
    private boolean getOptionFromGenerationMode(String generationMode) {
        return (generationMode != null && (generationMode.equals(RANDOM) || generationMode.equals(LIKE) || generationMode
                .equals(EXACT)));
    }

    /**
     * <p>selectCountry.</p>
     */
    public void selectCountry() {
        if (selectedCountryString != null) {
            for (CountryCode country : allCountries) {
                if (country.getPrintableName().equalsIgnoreCase(selectedCountryString)
                        || country.getIso().equalsIgnoreCase(selectedCountryString)
                        || country.getIso3().equalsIgnoreCase(selectedCountryString)) {
                    selectedCountry = country;
                    break;
                } else {
                    continue;
                }
            }
        } else {
            selectedCountry = null;
        }
    }

    /**
     * <p>Getter for the field <code>ddsMode</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getDdsMode() {
        if (this.ddsMode == null) {
            ddsMode = PreferenceService.getString("dds_mode");
            if (ddsMode == null) {
                log.error("DDS Mode is not set, please create a preference with name dds_mode. minimal will be used by default");
                this.ddsMode = MINIMAL;
            }
        }
        return ddsMode;
    }

    /**
     * <p>Setter for the field <code>ddsMode</code>.</p>
     *
     * @param ddsMode a {@link java.lang.String} object.
     */
    protected void setDdsMode(String ddsMode) {
        this.ddsMode = ddsMode;
    }

}
