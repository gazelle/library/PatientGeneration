package net.ihe.gazelle.patient;

import au.com.bytecode.opencsv.CSVReader;
import au.com.bytecode.opencsv.bean.ColumnPositionMappingStrategy;
import au.com.bytecode.opencsv.bean.CsvToBean;
import org.jboss.seam.annotations.Create;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;
import org.richfaces.event.FileUploadEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.lang.reflect.Field;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * <p>Abstract PatientImporter class.</p>
 *
 * @author aberge
 * @version $Id: $Id
 */
public abstract class PatientImporter<P extends AbstractPatient> implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 3269089717226439078L;

    public List<P> importedPatients;
    public List<String> attributesOrder;
    public List<String> availableAttributes;

    private static Logger log = LoggerFactory.getLogger(PatientImporter.class);

    /**
     * <p>getPatientClass.</p>
     *
     * @return a {@link java.lang.Class} object.
     */
    public abstract Class<P> getPatientClass();

    /**
     * <p>init.</p>
     */
    @Create
    public void init() {
        availableAttributes = new ArrayList<String>();
        Field[] fieldsFromSuperClass = AbstractPatient.class.getDeclaredFields();
        if (getPatientClass() != null) {
            Field[] fields = getPatientClass().getDeclaredFields();
            for (Field field : fields) {
                if (field.getType().equals(String.class)) {
                    availableAttributes.add(field.getName());
                }
            }
            for (Field field : fieldsFromSuperClass) {
                if (field.getType().equals(String.class)) {
                    availableAttributes.add(field.getName());
                }
            }
            Collections.sort(availableAttributes);
        }
    }

    /**
     * <p>patientFileUploadListener.</p>
     *
     * @param event a {@link org.richfaces.event.FileUploadEvent} object.
     */
    public void patientFileUploadListener(FileUploadEvent event) {
        if (attributesOrder == null || attributesOrder.isEmpty()) {
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, "Please first select the format of your CSV file");
        } else {
            ColumnPositionMappingStrategy<P> strategy = new ColumnPositionMappingStrategy<P>();
            strategy.setType(getPatientClass());
            CsvToBean<P> csv = new CsvToBean<P>();
            try {
                CSVReader csvReader = new CSVReader(new InputStreamReader(event.getUploadedFile().getInputStream(), StandardCharsets.UTF_8));
                String[] attributeList = attributesOrder.toArray(new String[attributesOrder.size()]);
                strategy.setColumnMapping(attributeList);
                importedPatients = csv.parse(strategy, csvReader);
                if (importedPatients != null && !importedPatients.isEmpty()) {
                    FacesMessages.instance().add(StatusMessage.Severity.INFO,
                            importedPatients.size() + " have been extracted from the file, choose the ones to import");
                } else {
                    FacesMessages.instance().add(StatusMessage.Severity.WARN, "No patient has been found in the uploaded file");
                }
            } catch (FileNotFoundException e) {
                FacesMessages.instance().add(
                        StatusMessage.Severity.ERROR, "Application has not been able to process the uploaded file: " + e.getMessage());
                log.error(e.getMessage(), e);
            } catch (RuntimeException e) {
                FacesMessages.instance().add(
                        StatusMessage.Severity.ERROR, "Application has not been able to process the uploaded file: " + e.getMessage());
                log.error(e.getMessage(), e);
            } catch (IOException e) {
               log.error(e.getMessage(), e);
            }
        }
    }

    /**
     * <p>Getter for the field <code>attributesOrder</code>.</p>
     *
     * @return a {@link java.util.List} object.
     */
    public List<String> getAttributesOrder() {
        return attributesOrder;
    }

    /**
     * <p>removePatientFromList.</p>
     *
     * @param selectedPatient a P object.
     */
    public void removePatientFromList(P selectedPatient) {
        importedPatients.remove(selectedPatient);
    }

    /**
     * <p>saveExtractedPatients.</p>
     */
    public void saveExtractedPatients() {
        Integer index = 0;
        for (P patient : importedPatients) {
            savePatient(patient);
            index ++;
        }
        FacesMessages.instance().add(StatusMessage.Severity.INFO, index + " patients have been stored in the database, you can consult them from the 'All patients' menu");
    }

    /**
     * <p>savePatient.</p>
     *
     * @param patient a P object.
     */
    public abstract void savePatient(P patient);

    /**
     * <p>Setter for the field <code>attributesOrder</code>.</p>
     *
     * @param attributesOrder a {@link java.util.List} object.
     */
    public void setAttributesOrder(List<String> attributesOrder) {
        if (this.attributesOrder == null) {
            this.attributesOrder = new ArrayList<String>();
        }
        this.attributesOrder = attributesOrder;
    }

    /**
     * <p>Getter for the field <code>importedPatients</code>.</p>
     *
     * @return a {@link java.util.List} object.
     */
    public List<P> getImportedPatients() {
        return importedPatients;
    }

    /**
     * <p>Getter for the field <code>availableAttributes</code>.</p>
     *
     * @return a {@link java.util.List} object.
     */
    public List<String> getAvailableAttributes() {
        return availableAttributes;
    }

    /**
     * <p>Setter for the field <code>availableAttributes</code>.</p>
     *
     * @param availableAttributes a {@link java.util.List} object.
     */
    public void setAvailableAttributes(List<String> availableAttributes) {
        this.availableAttributes = availableAttributes;
    }
}
