package net.ihe.gazelle.patient;

import javax.faces.model.SelectItem;
import java.util.List;

/**
 * <b>Class Description : </b>DemographicCodeProvider<br>
 * <br>
 *
 * @author Anne-Gaelle Berge / IHE-Europe development Project
 * @version 1.0 - 10/02/16
 */
public interface DemographicCodeProvider {

    /**
     * <p>getReligion.</p>
     *
     * @param patient a {@link net.ihe.gazelle.patient.AbstractPatient} object.
     * @return a {@link java.lang.String} object.
     */
    String getReligion(AbstractPatient patient);

    /**
     * <p>getRace.</p>
     *
     * @param patient a {@link net.ihe.gazelle.patient.AbstractPatient} object.
     * @return a {@link java.lang.String} object.
     */
    String getRace(AbstractPatient patient);

    /**
     * <p>getGender.</p>
     *
     * @param patient a {@link net.ihe.gazelle.patient.AbstractPatient} object.
     * @return a {@link java.lang.String} object.
     */
    String getGender(AbstractPatient patient);

    /**
     * <p>getCountry.</p>
     *
     * @param patient a {@link net.ihe.gazelle.patient.AbstractPatient} object.
     * @return a {@link java.lang.String} object.
     */
    String getCountry(AbstractPatient patient);

    /**
     * <p>getAllReligions.</p>
     *
     * @return : list of religions
     */
    List<SelectItem> getAllReligions();

    /**
     * <p>getAllRaces.</p>
     *
     * @return : list of races
     */
    List<SelectItem> getAllRaces();

    /**
     * <p>getAllGenders.</p>
     *
     * @return : list of genders
     */
    List<SelectItem> getAllGenders();

}
