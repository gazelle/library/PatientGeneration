package net.ihe.gazelle.patient.action;

import net.ihe.gazelle.patient.AbstractPatient;
import net.ihe.gazelle.patient.AddressType;
import net.ihe.gazelle.patient.PatientAddress;

import javax.faces.model.SelectItem;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by aberge on 28/06/17.
 *
 * @author aberge
 * @version $Id: $Id
 * @since 2.2.0
 */
public abstract class PatientAddressEditor {

    private PatientAddress addrToEdit;
    private static final List<SelectItem> availableAddressTypes;

    static{
        availableAddressTypes = new ArrayList<SelectItem>();
        for (AddressType type : AddressType.values()) {
            availableAddressTypes.add(new SelectItem(type, type.getLabel()));
        }
    }

    /**
     * <p>Getter for the field <code>addrToEdit</code>.</p>
     *
     * @return a {@link net.ihe.gazelle.patient.PatientAddress} object.
     */
    public PatientAddress getAddrToEdit() {
        return addrToEdit;
    }

    /**
     * <p>Setter for the field <code>addrToEdit</code>.</p>
     *
     * @param addrToEdit a {@link net.ihe.gazelle.patient.PatientAddress} object.
     */
    public void setAddrToEdit(PatientAddress addrToEdit) {
        this.addrToEdit = addrToEdit;
    }

    /**
     * <p>editAddress.</p>
     *
     * @param addr a {@link net.ihe.gazelle.patient.PatientAddress} object.
     * @return a boolean.
     */
    public boolean editAddress(PatientAddress addr){
        if (addrToEdit == null){
            return false;
        } else {
            return addr.getFakeId().equals(addrToEdit.getFakeId());
        }
    }

    /**
     * <p>Getter for the field <code>availableAddressTypes</code>.</p>
     *
     * @return a {@link java.util.List} object.
     */
    public List<SelectItem> getAvailableAddressTypes(){
        return this.availableAddressTypes;
    }

    /**
     * <p>createNewAddress.</p>
     *
     * @param patient a {@link net.ihe.gazelle.patient.AbstractPatient} object.
     */
    public void createNewAddress(AbstractPatient patient){
        addrToEdit = new PatientAddress();
        addrToEdit.setPatient(patient);
        patient.addPatientAddress(addrToEdit);
    }
}
