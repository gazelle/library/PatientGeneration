package net.ihe.gazelle.patient.util;

import net.ihe.gazelle.common.interfacegenerator.GenerateInterface;
import net.ihe.gazelle.patient.AbstractPatient;
import net.ihe.gazelle.patient.DemographicCodeProvider;
import net.ihe.gazelle.preferences.PreferenceService;
import org.jboss.resteasy.client.ClientExecutor;
import org.jboss.resteasy.client.ClientRequest;
import org.jboss.resteasy.client.ClientResponse;
import org.jboss.resteasy.client.core.executors.ApacheHttpClient4Executor;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.international.LocaleSelector;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.faces.model.SelectItem;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.ByteArrayInputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;

/**
 * <p>SVSConsumer class.</p>
 *
 * @author aberge
 * @version $Id: $Id
 */
@Name("svsConsumer")
@AutoCreate
@Scope(ScopeType.APPLICATION)
@GenerateInterface("SVSConsumerInterface")
public class SVSConsumer implements SVSConsumerInterface, DemographicCodeProvider {

    /**
     * Constant <code>DEFAULT_GENDER_OID="1.3.6.1.4.1.21367.101.101"</code>
     */
    private static final String DEFAULT_GENDER_OID = "1.3.6.1.4.1.21367.101.101";
    /**
     * Constant <code>DEFAULT_RACE_OID="1.3.6.1.4.1.21367.101.102"</code>
     */
    private static final String DEFAULT_RACE_OID = "1.3.6.1.4.1.21367.101.102";
    /**
     * Constant <code>DEFAULT_RELIGION_OID="1.3.6.1.4.1.21367.101.122"</code>
     */
    private static final String DEFAULT_RELIGION_OID = "1.3.6.1.4.1.21367.101.122";

    private String genderOid;
    private String raceOid;
    private String religionOid;

    private static final Comparator<SelectItem> CONCEPT_COMPARATOR = new Comparator<SelectItem>() {

        @Override
        public int compare(SelectItem item1, SelectItem item2) {
            return item1.getLabel().compareToIgnoreCase(item2.getLabel());
        }

    };
    private static final ClientExecutor CLIENT_EXECUTOR = new ApacheHttpClient4Executor();
    private static Logger log = LoggerFactory.getLogger(SVSConsumer.class);
    private String endpointUrl = null;

    private static Document parse(String document) throws Exception {
        ByteArrayInputStream bais = new ByteArrayInputStream(document.getBytes(StandardCharsets.UTF_8));

        // Rebuild the document
        DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
        builderFactory.setNamespaceAware(true);

        DocumentBuilder builder = builderFactory.newDocumentBuilder();
        Document documentDOM = builder.parse(bais);
        bais.close();
        return documentDOM;
    }

    /**
     * get the SVS Repository URL from the preferences of the applications
     *
     * @return
     */
    private String getEndpointUrl() {
        if (this.endpointUrl == null) {
            this.endpointUrl = PreferenceService.getString("svs_repository_url");
            if (this.endpointUrl == null) {
                log.error("svs_repository preference is missing set http://gazelle.ihe.net as default value");
                this.endpointUrl = "http://gazelle.ihe.net";
            }
        }
        return this.endpointUrl;
    }

    /**
     * returns the display name associated to the given code in the given value set
     */
    public String getDisplayNameForGivenCode(String valueSetId, String lang, String code) {
        if (valueSetId == null || valueSetId.isEmpty()) {
            return null;
        }
        if (code == null || code.isEmpty()) {
            return null;
        }
        ClientRequest request = new ClientRequest(getEndpointUrl().concat("/RetrieveValueSetForSimulator"), CLIENT_EXECUTOR);
        request.followRedirects(true);
        request.queryParameter("id", valueSetId);
        if (lang != null && !lang.isEmpty()) {
            request.queryParameter("lang", lang);
        }
        request.queryParameter("code", code);
        String displayName = code;
        ClientResponse<String> response = null;
        try {
            response = request.get(String.class);
            if (response.getStatus() == 200) {
                String xmlContent = response.getEntity();
                Document document = parse(xmlContent);
                NodeList concepts = document.getElementsByTagName("Concept");
                if (concepts.getLength() > 0) {
                    Node conceptNode = concepts.item(0);
                    NamedNodeMap attributes = conceptNode.getAttributes();
                    displayName = attributes.getNamedItem("displayName").getTextContent();
                }
            }
        } catch (Exception e) {
            log.error("Failed to get display name " + endpointUrl + " - " + valueSetId + " - " + lang + " - " + code, e);
        } finally {
            if (response != null) {
                try {
                    response.releaseConnection();

                } catch (Exception e2) {
                    log.debug("Failed to release connection", e2);
                }
            }
        }
        return displayName;
    }

    /**
     * returns the list of concepts available in a given value set for a given language
     */
    public List<SelectItem> getConceptsListFromValueSet(String valueSetId, String lang) {
        List<SelectItem> conceptsList = new ArrayList<SelectItem>();

        if (valueSetId != null && !valueSetId.isEmpty()) {
            ClientRequest request = new ClientRequest(getEndpointUrl().concat("/RetrieveValueSetForSimulator"), CLIENT_EXECUTOR);
            request.queryParameter("id", valueSetId);
            if (lang != null && !lang.isEmpty()) {
                request.queryParameter("lang", lang);
            }
            ClientResponse<String> response = null;
            try {
                response = request.get(String.class);
                if (response.getStatus() == 200) {
                    String xmlContent = response.getEntity();
                    Document document = parse(xmlContent);
                    NodeList concepts = document.getElementsByTagName("Concept");
                    if (concepts.getLength() > 0) {
                        for (int index = 0; index < concepts.getLength(); index++) {
                            Node conceptNode = concepts.item(index);
                            NamedNodeMap attributes = conceptNode.getAttributes();
                            conceptsList.add(new SelectItem(attributes.getNamedItem("code").getTextContent(), attributes
                                    .getNamedItem("displayName").getTextContent()));
                        }
                        Collections.sort(conceptsList, CONCEPT_COMPARATOR);
                        conceptsList.add(0, new SelectItem(null, "Please select..."));
                        return conceptsList;
                    }
                }
            } catch (Exception e) {
                log.error("Failed to get concept list " + getEndpointUrl() + " - " + valueSetId + " - " + lang, e);
            } finally {
                if (response != null) {
                    try {
                        response.releaseConnection();
                    } catch (Exception e2) {
                        log.debug("Failed to release connection", e2);
                    }
                }
            }
            return conceptsList;
        } else {
            return conceptsList;
        }
    }


    public String getReligion(AbstractPatient patient) {
        return getDisplayNameForGivenCode(getReligionOid(), LocaleSelector.instance()
                .getLanguage(), patient.getReligionCode());
    }


    public String getRace(AbstractPatient patient) {
        return getDisplayNameForGivenCode(getRaceOid(), LocaleSelector.instance()
                .getLanguage(), patient.getRaceCode());
    }


    public String getGender(AbstractPatient patient) {
        return getDisplayNameForGivenCode(getGenderOid(), LocaleSelector.instance()
                .getLanguage(), patient.getGenderCode());
    }


    public String getCountry(AbstractPatient patient) {
        if (patient.getCountryCode() != null) {
            Locale locale = new Locale(LocaleSelector.instance().getLanguage(), patient.getCountryCode());
            return locale.getDisplayCountry(locale);
        } else {
            return null;
        }
    }

    /**
     * <p>getAllReligions.</p>
     *
     * @return : list of religion
     */
    public List<SelectItem> getAllReligions() {
        return getConceptsListFromValueSet(getReligionOid(), LocaleSelector.instance()
                .getLanguage());
    }

    /**
     * <p>getAllRaces.</p>
     *
     * @return : list of race
     */
    public List<SelectItem> getAllRaces() {
        return getConceptsListFromValueSet(getRaceOid(), LocaleSelector.instance()
                .getLanguage());
    }

    /**
     * <p>getAllGenders.</p>
     *
     * @return : list of gender
     */
    public List<SelectItem> getAllGenders() {
        return getConceptsListFromValueSet(getGenderOid(), LocaleSelector.instance()
                .getLanguage());
    }

    public String getGenderOid() {
        if (genderOid == null || genderOid.isEmpty()){
            this.genderOid = DEFAULT_GENDER_OID;
        }
        return genderOid;
    }

    public void setGenderOid(String genderOid) {
        this.genderOid = genderOid;
    }

    public String getRaceOid() {
        if (raceOid == null || raceOid.isEmpty()){
            this.raceOid = DEFAULT_RACE_OID;
        }
        return raceOid;
    }

    public void setRaceOid(String raceOid) {
        this.raceOid = raceOid;
    }

    public String getReligionOid() {
        if (religionOid == null || religionOid.isEmpty()){
            this.religionOid = DEFAULT_RELIGION_OID;
        }
        return religionOid;
    }

    public void setReligionOid(String religionOid) {
        this.religionOid = religionOid;
    }
}
