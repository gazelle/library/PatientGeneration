From PatientGeneration:2.2.0

* Update the class which extends AbstractPatient, should now be something like

```java
@Entity
@Name("patient")
@DiscriminatorValue("pam_patient")
public class Patient extends AbstractPatient{}       
```

* Add the following in META-INF/hibernate.cfg.xml: ```<mapping class="net.ihe.gazelle.patient.PatientAddress"/>```
* Add the following in META-INF/persistent.xml: ```<jar-file>PatientGeneration-model.jar</jar-file>```
* Declare the module in the pom.xml of the EAR file
```xml
<ejbModule>
    <groupId>net.ihe.gazelle.maven</groupId>
    <artifactId>PatientGeneration-model</artifactId>
    <uri>PatientGeneration-model.jar</uri>
</ejbModule>
```
* In XHTML files, editAbstractPatientAddress.xhtml shall be included with ```<ui:param name="managerBean" value=#{TheNameOfYourBeanHere}/>```

* Applies migration script: patients are now stored in pat_patient and addresses in pat_patient_address